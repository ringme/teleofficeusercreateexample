#! /usr/bin/python
# -*- coding: utf-8 -*-

'''
(c) Dmitri Savolainen 2016-12-12
'''

import json
import requests
from ringmelib import RingmeTrustedApp, UnauthorizedException, BadResponseException, NotFoundException
import logging
import traceback
from conf import Conf
from random import choice


requests.packages.urllib3.disable_warnings()


class TeleoClientParams:

    def __init__(self, dealer_id, domain_id, greeting_id, trunk_id, pbx_name, email, web_addr, did_list=tuple()):
        self.dealer_id = dealer_id
        self.domain_id = domain_id
        self.greeting_id = greeting_id
        self.trunk_id = trunk_id
        self.pbx_name = pbx_name
        self.email = email
        self.web_addr = web_addr
        self.did_list = did_list


class TeleoNumberConfig:

    def __init__(self, trustedApp, clientParams):
        self.trustedApp = trustedApp
        self.clientParams = clientParams
        self.created_did = []

    def create_client(self):
        body = {
            "domain_id": self.clientParams.domain_id,
            "extension_len": 3,
            "dealer_id": self.clientParams.dealer_id,
            "email": self.clientParams.email,
            "disk_quota": 100 * 1024 * 1024,  # 100MB
            "name": self.clientParams.pbx_name,
            "extension_limit": 6,
        }
        newclient = trustedApp.post("admin/client/", body)
        created_id = newclient.get("id", None)
        if not isinstance(created_id, int):
            raise CreateException("Не получен коррекртный идентификатор клиента при создании АТС")
        return newclient

    def create_extension(self, client_id, ext_name, ext_type):
        body = {
            "name": "{ext_name}".format(ext_name=ext_name),
            "client_id": client_id,
            "type": ext_type,
            "status": "active"
        }
        extension_id = None
        ret = self.trustedApp.post("admin/extension/", body)
        extension_id = ret.get('id', None)

        if not isinstance(extension_id, int):
            raise CreateException("не удалось создать добавочный {ext_name}({ext_type}) для клиента с идентификатором {client_id}".format(ext_name=ext_name, ext_type=ext_type, client_id=client_id))
        return extension_id

    def create_client_user(self, client_id, name=None, login=None, email=None, user_pass=None):
        if not name:
            name = self.clientParams.pbx_name
        if not login:
            login = self.clientParams.pbx_name
        if not email:
            email = self.clientParams.email
        if not user_pass:
            user_pass = ''.join(choice("234679ADEFGHJKLMNPRTUWabdefghijkmnpqrstuwy") for _ in range(10))

        body = {
            "name": name,
            "login": login,
            "password": user_pass,
            "email": email,
        }

        ret = self.trustedApp.post("/client/{client_id}/user/client/".format(client_id=client_id), body)
        user_id = ret.get('id', None)

        if not isinstance(user_id, int):
            raise CreateException("не удалось создать пользователя для клиента АТС {client_id}".format(client_id=client_id))

        # для логина по ссылке из админского интерфейса
        body = {
            "hidden_json": "{{\"interface\": \"<a target='_blank' href='{web_addr}/login/authorize_as/?username={username}'>Интерфейс Клиента</a>\"}}".format(username=login, web_addr=self.clientParams.web_addr),
        }
        trustedApp.put("/admin/client/{client_id}".format(client_id=client_id), body)

        return ret

    def configure_ivr(self, ivr_id, didlist=None):

        if not didlist:
            didlist = self.clientParams.did_list

        body = {  # создать главный контекст
            "allow_any_dial": False,
            "inter_digit_timeout": 2000,
            "name": "Главный Контекст",
            "timeout": 5000
        }

        ret = self.trustedApp.post("/extension/{ivr_id}/ivr/context/".format(ivr_id=ivr_id), body)
        main_context_id = ret.get("id", "")
        if not isinstance(main_context_id, int):
            raise CreateException("не удалось создать главный контекст")

        body = {  # привязать главный контекст к добавочному
            "sleep_time": 1000,
            "entry_context": main_context_id,
            "lifetime": 600
        }
        self.trustedApp.put("/extension/{ivr_id}/ivr/".format(ivr_id=ivr_id), body)

        # создать временной контекст - рабочий
        body = {
            "allow_any_dial": True,
            "inter_digit_timeout": 2000,
            "name": "Круглосуточно",
            "timeout": 5000
        }
        ret = self.trustedApp.post("/extension/{ivr_id}/ivr/context/".format(ivr_id=ivr_id), body)
        work_context_id = ret.get("id", "")
        if not isinstance(work_context_id, int):
            raise CreateException("не удалось создать временной(рабочий) контекст")

        # создать номерной контекст
        body = {
            "allow_any_dial": True,
            "inter_digit_timeout": 2000,
            "name": "Все номера",
            "timeout": 5000
        }
        ret = self.trustedApp.post("/extension/{ivr_id}/ivr/context/".format(ivr_id=ivr_id), body)
        number_context_id = ret.get("id", "")
        if not isinstance(number_context_id, int):
            raise CreateException("не удалось создать рабочий контекст")

        # добавить все имеющиеся диды  в опцию start главного контекста с правилом перехода в номерной контекст
        for did in didlist:
            body = {
                "match_variable_name": "called_did",
                "match_variable_value": did,
                "action": "jump_to_context",
                "context": number_context_id,
                "context_option": "start",
            }
            self.trustedApp.post("extension/{ivr_id}/ivr/context/{main_context_id}/options/start/rules/".format(ivr_id=ivr_id, main_context_id=main_context_id), body)

        # добавить правило перехода во временной контект из номерного
        body = {
            "action": "jump_to_context",
            "context": work_context_id,
            "context_option": "start",
        }
        self.trustedApp.post("extension/{ivr_id}/ivr/context/{number_context_id}/options/start/rules/".format(ivr_id=ivr_id, number_context_id=number_context_id), body)

        body = {  # мелодия опции start рабочего контекста
            "sound": self.clientParams.greeting_id,
            "sound_type": "background",
            "action": "play_sound"
        }
        ret = self.trustedApp.post("extension/{ivr_id}/ivr/context/{work_context_id}/options/start/rules/".format(ivr_id=ivr_id, work_context_id=work_context_id), body)
        main_context_rule_id = ret.get('id', None)
        if not isinstance(main_context_rule_id, int):
            raise CreateException("не удалось создать правило рабочего контекста")

        body = {  # ничего не выбрано - на 101
            "action": "transfer",
            "transfer_dst": "101",
            "name": "{0}".format(main_context_rule_id)
        }
        self.trustedApp.post("extension/{ivr_id}/ivr/context/{work_context_id}/options/timeout/rules/".format(ivr_id=ivr_id, work_context_id=work_context_id), body)

        body = {  # ошибка выбора - на 101
            "action": "transfer",
            "transfer_dst": "101",
            "name": "{0}".format(main_context_rule_id)
        }
        self.trustedApp.post("extension/{ivr_id}/ivr/context/{work_context_id}/options/invalid/rules/".format(ivr_id=ivr_id, work_context_id=work_context_id), body)

        body = {
            "digits": "0"
        }
        self.trustedApp.post("/extension/{ivr_id}/ivr/context/{work_context_id}/options/".format(ivr_id=ivr_id, work_context_id=work_context_id), body)
        body = {
            "action": "transfer",
            "transfer_dst": "199"
        }
        self.trustedApp.post("extension/{ivr_id}/ivr/context/{work_context_id}/options/0/rules/".format(ivr_id=ivr_id, work_context_id=work_context_id), body)

        body = {
            "digits": "1"
        }
        self.trustedApp.post("/extension/{ivr_id}/ivr/context/{work_context_id}/options/".format(ivr_id=ivr_id, work_context_id=work_context_id), body)
        body = {
            "action": "transfer",
            "transfer_dst": "101"
        }
        self.trustedApp.post("extension/{ivr_id}/ivr/context/{work_context_id}/options/1/rules/".format(ivr_id=ivr_id, work_context_id=work_context_id), body)

        body = {
            "digits": "2"
        }
        self.trustedApp.post("/extension/{ivr_id}/ivr/context/{work_context_id}/options/".format(ivr_id=ivr_id, work_context_id=work_context_id), body)
        body = {
            "action": "transfer",
            "transfer_dst": "102"
        }
        self.trustedApp.post("extension/{ivr_id}/ivr/context/{work_context_id}/options/2/rules/".format(ivr_id=ivr_id, work_context_id=work_context_id), body)

        body = {
            "digits": "3"
        }
        self.trustedApp.post("/extension/{ivr_id}/ivr/context/{work_context_id}/options/".format(ivr_id=ivr_id, work_context_id=work_context_id), body)
        body = {
            "action": "transfer",
            "transfer_dst": "103"
        }
        self.trustedApp.post("extension/{ivr_id}/ivr/context/{work_context_id}/options/3/rules/".format(ivr_id=ivr_id, work_context_id=work_context_id), body)

    def configure_fax(self, fax_id, email=None):
        if not email:
            email = self.clientParams.email
        body = {
            "fax_mailto": email
        }
        ret = self.trustedApp.put("/extension/{extension_id}/fax/".format(extension_id=fax_id), body)
        if not ret.get("fax_mailto", None):
            raise CreateException("не сконфигурировать факс {extension_id}".format(extension_id=fax_id))

    def configure_phone(self, ext_id, email=None, devpass=None):
        if not devpass:
            devpass = ''.join(choice("234679ADEFGHJKLMNPRTUWabdefghijkmnpqrstuwy") for _ in range(10))
        if not email:
            email = self.clientParams.email
        body = {
            "vm_mailto": email,
            "vm_enabled": True,
            "record_enabled": False,
            "vm_attach_file": True,
            "password": devpass
        }
        ret = self.trustedApp.put("/extension/{extension_id}/phone/".format(extension_id=ext_id), body)
        if not ret.get("vm_mailto", None):
            raise CreateException("не сконфигурировать добавочный {extension_id}".format(extension_id=ext_id))

    def add_did(self, ext_id, didlist=None):
        if not didlist:
            didlist = self.clientParams.did_list
        for did in didlist:
            body = {
                "name": did,
                "extension_id": ext_id,
                "trunk_id": self.clientParams.trunk_id
            }
            ret = self.trustedApp.post("/admin/did/", body)
            if ret and ret.get("id", None):
                self.created_did.append(ret["id"])

    def get_pbx_config(self, pbx_client_id):
        description = {}
        ret_client = self.trustedApp.get('admin/client/{pbx_client_id}'.format(pbx_client_id=pbx_client_id))
        description["client_info"] = {"id": ret_client["id"]}
        did_list = {}
        for did in self.trustedApp.get('/client/{pbx_client_id}/did/'.format(pbx_client_id=pbx_client_id)):
            did_ext_id = did.get("extension_id", None)
            if isinstance(did_ext_id, int):
                if did_ext_id not in did_list:
                    did_list[did_ext_id] = []
                did_list[did_ext_id].append(did["name"])
        ret_ext = self.trustedApp.get('/client/{pbx_client_id}/extension/'.format(pbx_client_id=pbx_client_id))
        description["extension_list"] = []
        for extension in ret_ext:
            ext_id = extension["id"]
            ext_dict = {}
            ext_dict["id"] = ext_id
            ext_dict["type"] = extension["type"]
            ext_dict["name"] = extension["name"]
            if extension["type"] == "phone":
                ret_sip_pass = self.trustedApp.get('/admin/extension/{ext_id}/sip_auth_info/'.format(ext_id=ext_id))
                ext_dict["sip_pass"] = ret_sip_pass["password"]
            if ext_id in did_list:
                ext_dict["did_list"] = did_list[ext_id]
            description["extension_list"].append(ext_dict)
        description["user_info"] = dict()
        user_ret = self.trustedApp.get("/client/{pbx_client_id}/user/client/".format(pbx_client_id=pbx_client_id))
        if isinstance(user_ret, list) and len(user_ret) > 0:
            description["user_info"]["login"] = user_ret[0]["login"]
        return description

    def roll_back_creation(self, pbx_client_id):
        self.trustedApp.delete('/admin/client/{pbx_client_id}'.format(pbx_client_id=pbx_client_id))
        for did_id in self.created_did:
            self.trustedApp.delete('/admin/did/{did_id}'.format(did_id=did_id))


class CreateException(Exception):
    pass


trustedApp = RingmeTrustedApp(Conf.API_HOST, Conf.API_PATH, Conf.APP_ID, Conf.APP_SECRET)
logging.getLogger("requests").setLevel(logging.ERROR)
logging.getLogger().setLevel(logging.INFO)

ret_dict = {"result": "failed", "description": "unknown error"}

# специфические параметры нового клиента (тут законстанчены для примера)
new_client_params = {
    "pbx_name": "SomeTeleoffice",
    "email": "foo@foo.com",
    "did_list": ('78121234567', '74957654321'),
    "web_addr": "https://teleo_addr.domain"  # для формирования ссылки на страницу клиентского интерфейса из административного веб интрефейса
}

teleoParams = TeleoClientParams(
    Conf.RINGME_DEALER_ID,
    Conf.RINGME_SIP_DOMAIN_ID,
    Conf.DEFAULT_GREETING_ID,
    Conf.RINGME_TRANK_ID,
    new_client_params['pbx_name'],
    new_client_params['email'],
    new_client_params['web_addr'],
    new_client_params['did_list']
)
teleocfg = TeleoNumberConfig(trustedApp, teleoParams)
try:
    pbx_client_id = None
    clientCreated = teleocfg.create_client()
    pbx_client_id = clientCreated["id"]

    ext_id = teleocfg.create_extension(pbx_client_id, '101', 'phone')
    teleocfg.configure_phone(ext_id)

    ext_id = teleocfg.create_extension(pbx_client_id, '102', 'phone')
    teleocfg.configure_phone(ext_id)

    ext_id = teleocfg.create_extension(pbx_client_id, '103', 'phone')
    teleocfg.configure_phone(ext_id)

    ivr_id = teleocfg.create_extension(pbx_client_id, '099', 'ivr')
    teleocfg.add_did(ivr_id)
    teleocfg.configure_ivr(ivr_id)

    fax_id = teleocfg.create_extension(pbx_client_id, '199', 'fax')
    teleocfg.configure_fax(fax_id)

    user_pass = ''.join(choice("234679ADEFGHJKLMNPRTUWabdefghijkmnpqrstuwy") for _ in range(8))
    new_user_info = teleocfg.create_client_user(pbx_client_id, user_pass=user_pass)
    user_id = new_user_info['id']

    config_desctiption = teleocfg.get_pbx_config(pbx_client_id)
    config_desctiption["user_info"]["password"] = user_pass
    ret_dict = {"result": "ok", "description": config_desctiption}

except Exception as e:
    if isinstance(pbx_client_id, int):
        teleocfg.roll_back_creation(pbx_client_id)
    if isinstance(e, UnauthorizedException):
        ret_dict["description"] = "Не пройти OAuth2.0 авторизацию"
    elif isinstance(e, CreateException) or isinstance(e, BadResponseException) or isinstance(e, NotFoundException):
        ret_dict["description"] = str(e)
    else:
        ret_dict["description"] = "{type}: {error}; traceback: {traceback}".format(type=type(e).__name__, error=e, traceback=traceback.format_exc())

print "START_JSON_DOC"
print json.dumps(ret_dict, indent=4, sort_keys=True, ensure_ascii=True).decode("unicode-escape")
